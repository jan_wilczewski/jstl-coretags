<%--
  Created by IntelliJ IDEA.
  User: jan_w
  Date: 10.10.2017
  Time: 08:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Core Tag Example</title>
</head>
<body>

<c:set var="income" value="${4000*4}"/>
<p> Your income is: <c:out value="${income}"/> </p>
<c:choose>
    <c:when test="${income <= 1000}">
        Income is not good.
    </c:when>
    <c:when test="${income > 10000}">
        Icnome is very good.
    </c:when>
    <c:otherwise>
        Income is unknown...
    </c:otherwise>
</c:choose>

</body>
</html>
