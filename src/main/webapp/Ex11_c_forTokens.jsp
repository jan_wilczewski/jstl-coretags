<%--
  Created by IntelliJ IDEA.
  User: jan_w
  Date: 10.10.2017
  Time: 09:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Core Tag Example</title>
</head>
<body>

<c:forTokens items="Adam-Jan-Kuba" delims="-" var="name">
    <c:out value="${name}"/><p>
</c:forTokens>
</body>
</html>
