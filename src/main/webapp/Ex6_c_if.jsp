<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: jan_w
  Date: 10.10.2017
  Time: 08:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Core Tag Example</title>
</head>
<body>

<c:set var="income" value="${4000*4}"/>
<c:if test="${income > 8000}">
    <p> My income is: <c:out value="${income}"/> </p>
</c:if>


</body>
</html>
