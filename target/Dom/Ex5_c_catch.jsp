<%--
  Created by IntelliJ IDEA.
  User: jan_w
  Date: 10.10.2017
  Time: 08:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Core Tag Example</title>
</head>
<body>

<c:catch var="catchTheException">
    <% int x = 2/0; %>
</c:catch>

<c:if test="${catchTheException != null}">
    <p> The type of exception is: ${catchTheException} <br/>
    There is ane exception: ${catchTheException.message}</p>

</c:if>

</body>
</html>
