<%--
  Created by IntelliJ IDEA.
  User: jan_w
  Date: 10.10.2017
  Time: 09:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Core Tag Example</title>
</head>
<body>

<h1> JSTL c:when, c:otherwise, c:choose </h1>

<c:set value="10" var="num"></c:set>
<c:choose>
    <c:when test="${num % 2 == 0}">
        <c:out value="${num} is even number"> </c:out>
    </c:when>
    <c:otherwise>
        <c:out value="${num} is odd number"></c:out>
    </c:otherwise>
</c:choose>

</body>
</html>
